import "./Admin.scss";
import {FiMoreVertical, FiPlus, FiSearch, FiTrash2} from "react-icons/fi";
import {useState} from "react";
import DataTable from 'react-data-table-component';
import AddUser from "../../components/AddUser/AddUser";

const Admin = () => {
    const [isOpen, setIsOpen] = useState(false);

    const handleClose = () => {
        setIsOpen(false);
    }

    const CustomIconAction = ({}) => (
        <div>
            <span
                className="IconDataTable"
                style={{
                    marginRight: "8px",
                    padding: "2px 4px",
                    cursor: "pointer"
            }}>
                <FiMoreVertical size={16} color={"gray"}/>
            </span>
                <span
                    className="IconDataTable"
                    style={{
                        padding: "2px 4px",
                        cursor: "pointer"
                    }}>
                <FiTrash2 size={16} color={"gray"}/>
            </span>
        </div>
    )
    const columns = [
        {
            name: 'Username',
            selector: row => row.name,
        },
        {
            name: 'Email',
            selector: row => row.email,
        },
        {
            name: 'Device',
            selector: row => row.device,
        },
        {
            name: 'Admin',
            selector: row => row.role,
        },
        {
            name: 'Action',
            selector: row => <CustomIconAction/>,
        },
    ];
    const data = [
        {
            id: 1,
            name: 'Nguyễn Hoàng Triều',
            email: 'nhtrieu@gmail.com',
            device: '0',
            role: 'admin',
            action: 'edit'
        },
        {
            id: 2,
            name: 'Tạ minh đức',
            email: 'tmduc@gmail.com',
            device: '2',
            Admin: 'user',
            action: 'edit'
        },
        {
            id: 1,
            name: 'Nguyễn Hoàng Triều',
            email: 'nhtrieu@gmail.com',
            device: '0',
            role: 'admin',
            action: 'edit'
        },
        {
            id: 2,
            name: 'Tạ minh đức',
            email: 'tmduc@gmail.com',
            device: '2',
            role: 'user',
            action: 'edit'
        },
        {
            id: 1,
            name: 'Nguyễn Hoàng Triều',
            email: 'nhtrieu@gmail.com',
            device: '0',
            role: 'admin',
            action: 'edit'
        },
        {
            id: 2,
            name: 'Tạ minh đức',
            email: 'tmduc@gmail.com',
            device: '2',
            role: 'user',
            action: 'edit'
        },
        {
            id: 1,
            name: 'Nguyễn Hoàng Triều',
            email: 'nhtrieu@gmail.com',
            device: '0',
            role: 'admin',
            action: 'edit'
        },
        {
            id: 2,
            name: 'Tạ minh đức',
            email: 'tmduc@gmail.com',
            device: '2',
            role: 'user',
            action: 'edit'
        },
        {
            id: 1,
            name: 'Nguyễn Hoàng Triều',
            email: 'nhtrieu@gmail.com',
            device: '0',
            role: 'admin',
            action: 'edit'
        },
        {
            id: 2,
            name: 'Tạ minh đức',
            email: 'tmduc@gmail.com',
            device: '2',
            role: 'user',
            action: 'edit'
        },
        {
            id: 1,
            name: 'Nguyễn Hoàng Triều',
            email: 'nhtrieu@gmail.com',
            device: '0',
            role: 'admin',
            action: 'edit'
        },
        {
            id: 2,
            name: 'Tạ minh đức',
            email: 'tmduc@gmail.com',
            device: '2',
            role: 'user',
            action: 'edit'
        },

    ]

    return (
        <>
            <div className="Admin">
                <div className="Admin_Container">
                    <div className="Admin_Container_Header">
                        <h2 className="Admin_Container_Header_Title">Users</h2>
                        <div className="Admin_Container_Header_Tool">
                            <div className="Admin_Container_Header_Tool_Search">
                                <input type="text" placeholder="User's name..."/>
                                <FiSearch/>
                            </div>
                            <div className="Admin_Container_Header_Tool_Add">
                                <div onClick={() => setIsOpen(true)} className="Admin_Container_Header_Tool_Add_Btn">
                                    <FiPlus/>
                                </div>
                                {isOpen ?
                                    <div className="Admin_Container_Header_Tool_Add_Popup">
                                        <AddUser handleClose={handleClose}/>
                                    </div> : <></>
                                }
                            </div>
                        </div>
                    </div>
                    <div className="Admin_Body">
                        <DataTable
                            columns={columns}
                            data={data}
                            pagination
                            fixedHeader={true}
                            fixedHeaderScrollHeight={"400px"}
                            selectableRows
                        />
                    </div>
                </div>
            </div>
        </>
    );
};
export default Admin;