import "./Control.scss";
import {FiMinus, FiPlus, FiPower} from "react-icons/fi";
import {useState} from "react";

const Control = () => {
    const [isRunning, setIsRunning] = useState(false);
    const [colors, setColors] = useState({
        red: 0,
        green: 0,
        blue: 0
    });
    const [mode, setMode] = useState("flash");

    const handleColorChange = (event, name) => {
        setColors({
            ...colors,
            [name]: event.target.value
        });
    };

    const toggleAnimation = () => {
        setIsRunning(!isRunning);
    };
    return (
        <>
        <div className="Control">
            <div className="Control_Container">
                <h2>Control</h2>
                <div className="Control_Container_Light">

                </div>
                <div className="Control_Container_Dashboard2">
                    <div onClick={toggleAnimation}
                         className={`Control_Container_Dashboard2_Btn ${isRunning ? 'Checked' : ''}`}>
                        <FiPower size={20} color={isRunning ? '#02ff02' : 'red'}/>
                    </div>
                    <div className="Control_Container_Dashboard2_Btn">
                        Mode
                    </div>
                </div>
                <div className="Control_Container_Dashboard3">
                    <div onClick={()=> setMode("flash")} className="Control_Container_Dashboard3_Btn">
                        Flash
                    </div>
                    <div onClick={()=> setMode("strobe")} className="Control_Container_Dashboard3_Btn">
                        Strobe
                    </div>
                    <div onClick={()=> setMode("fade")} className="Control_Container_Dashboard3_Btn">
                        Fade
                    </div>
                </div>
                <div className="Control_Container_Dashboard3">
                    <div className="Control_Container_Dashboard3_Btn">
                        R
                        <input type="color"
                               onChange={(e) => handleColorChange(e, "red")}
                        />
                    </div>
                    <div className="Control_Container_Dashboard3_Btn">
                        G
                        <input type="color"
                               onChange={(e) => handleColorChange(e, "green")}
                        />
                    </div>
                    <div className="Control_Container_Dashboard3_Btn">
                        B
                        <input type="color"
                               onChange={(e) => handleColorChange(e, "blue")}
                        />
                    </div>
                </div>
                <div className="Control_Container_Dashboard1">
                    <div className="Control_Container_Dashboard1_Btn">
                        <FiMinus/>
                    </div>
                    <div className="Control_Container_Dashboard1_Btn">
                        Brightness
                    </div>
                    <div className="Control_Container_Dashboard1_Btn">
                        <FiPlus/>
                    </div>
                </div>
            </div>
        </div>
        </>
    );
};

export default Control;