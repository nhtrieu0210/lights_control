import "./Settings.scss";
import Profile from "../../assets/images/profile-1.png";
import {FiGlobe, FiMoon, FiSettings, FiShield, FiUser, FiUsers} from "react-icons/fi";

const Settings = () => {
    return (
        <>
            <div className="Settings">
                <div className="Settings_Heading">
                    Settings Detail
                </div>
                <div className="Settings_Avt">
                    <div className="Settings_Avt_Image">
                        <img src={Profile} alt=""/>
                        <div className="Settings_Avt_Image_Btn">
                            <FiSettings/>
                        </div>
                    </div>
                    <div className="Settings_Avt_Name">
                        Nguyễn Hoàng Triều
                    </div>
                </div>
                <div className="Settings_Detail">
                    <div className="Settings_Detail_List">
                        <div className="Settings_Detail_List_Item">
                            <div className="Settings_Detail_List_Item_Icon">
                                <FiUser/>
                            </div>
                            <div className="Settings_Detail_List_Item_Text">
                                User name
                            </div>
                        </div>
                        <div className="Settings_Detail_List_Item">
                            <div className="Settings_Detail_List_Item_Icon">
                                <FiShield/>
                            </div>
                            <div className="Settings_Detail_List_Item_Text">
                                Password
                            </div>
                        </div>
                        <div className="Settings_Detail_List_Item">
                            <div className="Settings_Detail_List_Item_Icon">
                                <FiGlobe/>
                            </div>
                            <div className="Settings_Detail_List_Item_Text">
                                Language
                            </div>
                        </div>
                        <div className="Settings_Detail_List_Item">
                            <div className="Settings_Detail_List_Item_Icon">
                                <FiMoon/>
                            </div>
                            <div className="Settings_Detail_List_Item_Text">
                                Mode
                            </div>
                        </div>
                        <div className="Settings_Detail_List_Item">
                            <div className="Settings_Detail_List_Item_Icon">
                                <FiUsers />
                            </div>
                            <div className="Settings_Detail_List_Item_Text">
                                Admin
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Settings;