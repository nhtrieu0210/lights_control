import {BrowserView, MobileView} from "react-device-detect";

import {useContext, useEffect, useState} from "react";

import {FiEye, FiEyeOff, FiLock, FiUser} from "react-icons/fi";
import {Link, useNavigate} from "react-router-dom";
import Slogan from "./Slogan";

import  "./Auth.scss";
import  "./AuthMobile.scss";
import {handleValidatePassword, handleValidateUsername} from "../../Validation";
import {AuthContext} from "../../AuthContext";
import SloganMobile from "./SloganMobile";


const Login = () => {
    const navigate = useNavigate();
    const [formData, setFormData] = useState({
        userName: "",
        password: "",
    });
    const [errors, setErrors] = useState({
        userName: "",
        password: "",
    });
    const handleChange = (e, name) => {
        let value = e.target.value;
        setFormData({
            ...formData,
            [name]: value,
        });
    };

    const [openPassword, setOpenPassword] = useState(false);
    const handleShowPassword = () => {
        setOpenPassword(!openPassword);
    }

    const [saveAccount, setSaveAccount] = useState(false);
    const handleSaveAccount = (e) => {
        setSaveAccount(e.target.checked);
    }

    const authContext = useContext(AuthContext);

    const handleLogin = () => {
        let usernameValidation = handleValidateUsername(formData.userName);
        let passwordValidation = handleValidatePassword(formData.password);
        setErrors({
            userName: usernameValidation,
            password: passwordValidation
        })
        if (errors.userName || errors.password) {
            return;
        }
        if(formData.userName === 'admin1' && formData.password === 'admin1') {
            authContext.Login(saveAccount);
            navigate("/");
        }
    }

    useEffect(() => {
        const handleKeyPress = (event) => {
            if (event.key === 'Enter') {
                handleLogin();
            }
        };
        document.addEventListener('keypress', handleKeyPress);

        return () => {
            document.removeEventListener('keypress', handleKeyPress);
        };
    }, [formData]);

    return (
        <>
            <BrowserView>
                <div className="Auth">
                    <div className="Auth_Container">
                        <Slogan />
                        <div className="Auth_Container_Form">
                            <div className="Auth_Container_Form_Header">
                                <h1>Đăng nhập</h1>
                            </div>
                            <div className="Auth_Container_Form_Body">
                                <div className="Auth_Container_Form_Body_Item">
                                    <div className={`Auth_Container_Form_Body_Item_Content ${errors.userName ? 'Error' : ''}`}>
                                        <div className="Auth_Container_Form_Body_Item_Content_Icon">
                                            <FiUser color={`${errors.userName ? 'red' : 'black'}`} size={24}/>
                                        </div>
                                        <div className="Auth_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type="text"
                                                placeholder="Tên đăng nhập"
                                                onChange={(e) => handleChange(e,"userName")}
                                            />
                                        </div>
                                    </div>
                                    <div className="Auth_Container_Form_Body_Item_Validate">
                                        {errors.userName}
                                    </div>
                                </div>
                                <div className="Auth_Container_Form_Body_Item">
                                    <div className={`Auth_Container_Form_Body_Item_Content ${errors.password ? 'Error' : ''}`}>
                                        <div className="Auth_Container_Form_Body_Item_Content_Icon">
                                            <FiLock color={`${errors.password ? 'red' : 'black'}`} size={24}/>
                                        </div>
                                        <div className="Auth_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type={openPassword ? "text" : "password"}
                                                placeholder="Mật khẩu"
                                                onChange={(e) => handleChange(e,"password")}
                                            />
                                        </div>
                                        <div onClick={() => handleShowPassword()}
                                             className="Auth_Container_Form_Body_Item_Content_IconEye">
                                            {openPassword ? <FiEye size={20}/> :
                                                <FiEyeOff size={18}/>}
                                        </div>
                                    </div>
                                    <div className="Auth_Container_Form_Body_Item_Validate">
                                        {errors.password}
                                    </div>
                                </div>
                                <div className="Auth_Container_Form_Body_Item">
                                    <div className="Auth_Container_Form_Body_Item_Save">
                                        <input
                                            onClick={(e) => handleSaveAccount(e)}
                                            type="checkbox"/>
                                        <div>Lưu đăng nhập</div>
                                    </div>
                                </div>
                            </div>
                            <div className="Auth_Container_Form_Footer">
                                <button onClick={() => handleLogin()}>
                                    Đăng nhập
                                </button>
                                <div className="Auth_Container_Form_Footer_Choice">
                                <Link to="/register">Đăng ký tài khoản</Link>
                                    <Link to="/forgot-password">Quên mật khẩu?</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </BrowserView>
            <MobileView>
                    <div className="AuthMobile">
                        <div className="AuthMobile_Container">
                            <SloganMobile />
                            <div className="AuthMobile_Container_Form">
                                <div className="AuthMobile_Container_Form_Header">
                                    <h1>Đăng nhập</h1>
                                </div>
                                <div className="AuthMobile_Container_Form_Body">
                                    <div className="AuthMobile_Container_Form_Body_Item">
                                        <div
                                            className={`AuthMobile_Container_Form_Body_Item_Content ${errors.userName ? 'Error' : ''}`}>
                                            <div className="AuthMobile_Container_Form_Body_Item_Content_Icon">
                                                <FiUser color={`${errors.userName ? 'red' : 'black'}`} size={24}/>
                                            </div>
                                            <div className="AuthMobile_Container_Form_Body_Item_Content_Input">
                                                <input
                                                    type="text"
                                                    placeholder="Tên đăng nhập"
                                                    onChange={(e) => handleChange(e, "userName")}
                                                />
                                            </div>
                                        </div>
                                        <div className="AuthMobile_Container_Form_Body_Item_Validate">
                                            {errors.userName}
                                        </div>
                                    </div>
                                    <div className="AuthMobile_Container_Form_Body_Item">
                                        <div
                                            className={`AuthMobile_Container_Form_Body_Item_Content ${errors.password ? 'Error' : ''}`}>
                                            <div className="AuthMobile_Container_Form_Body_Item_Content_Icon">
                                                <FiLock color={`${errors.password ? 'red' : 'black'}`} size={24}/>
                                            </div>
                                            <div className="AuthMobile_Container_Form_Body_Item_Content_Input">
                                                <input
                                                    type={openPassword ? "text" : "password"}
                                                    placeholder="Mật khẩu"
                                                    onChange={(e) => handleChange(e, "password")}
                                                />
                                            </div>
                                            <div onClick={() => handleShowPassword()}
                                                 className="AuthMobile_Container_Form_Body_Item_Content_IconEye">
                                                {openPassword ? <FiEye size={20}/> :
                                                    <FiEyeOff size={18}/>}
                                            </div>
                                        </div>
                                        <div className="AuthMobile_Container_Form_Body_Item_Validate">
                                            {errors.password}
                                        </div>
                                    </div>
                                    <div className="AuthMobile_Container_Form_Body_Item">
                                        <div className="AuthMobile_Container_Form_Body_Item_Save">
                                            <input onClick={(e) => handleSaveAccount(e)} type="checkbox"/>
                                            <div>Lưu đăng nhập</div>
                                        </div>
                                    </div>
                                </div>
                                <div className="AuthMobile_Container_Form_Footer">
                                    <button onClick={() => handleLogin()}>
                                        Đăng nhập
                                    </button>
                                    <div className="AuthMobile_Container_Form_Footer_Choice">
                                        <Link to="/register">Đăng ký tài khoản</Link>
                                        <Link to="/forgot-password">Quên mật khẩu?</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            </MobileView>
        </>
    )
}

export default Login;