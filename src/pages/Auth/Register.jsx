import {BrowserView, MobileView} from "react-device-detect";
import {FiEye, FiEyeOff, FiLock, FiMail, FiUser, FiUserPlus} from "react-icons/fi";
import {Link, useNavigate} from "react-router-dom";
import {useState} from "react";
import Slogan from "./Slogan";
import {
    handleValidateConfirmPassword,
    handleValidateEmail,
    handleValidateName, handleValidatePassword,
    handleValidateUsername
} from "../../Validation";
import SloganMobile from "./SloganMobile";

const Register = () => {
    const navigate = useNavigate();
    const [formData, setFormData] = useState({
        fullName: "",
        userName: "",
        email: "",
        password: "",
        confirmPassword: "",
    });
    const [errors, setErrors] = useState({
        fullName: "",
        userName: "",
        email: "",
        password: "",
        confirmPassword: "",
    });

    const [openPassword, setOpenPassword] = useState(false);
    const handleShowPassword = () => {
        setOpenPassword(!openPassword);
    }

    const handleChange = (e, name) => {
        let value = e.target.value;
        setFormData({
            ...formData,
            [name] : value
        })
    }
    const handleSubmit = () => {
        console.log(formData)
        const validateName = handleValidateName(formData.fullName);
        const validateUserName = handleValidateUsername(formData.userName);
        const validateEmail = handleValidateEmail(formData.email);
        const validatePassword = handleValidatePassword(formData.password);
        const validateConfirmPassword = handleValidateConfirmPassword(formData.password, formData.confirmPassword);
        setErrors({
            fullName: validateName,
            userName: validateUserName,
            email: validateEmail,
            password: validatePassword,
            confirmPassword: validateConfirmPassword
        })
    }

    return (
        <>
            <BrowserView>
                <div className="Auth">
                    <div className="Auth_Container">
                        <Slogan />
                        <div className="Auth_Container_Form">
                            <div className="Auth_Container_Form_Header">
                                <h1>Đăng ký</h1>
                            </div>
                            <div className="Auth_Container_Form_Body">
                                <div className="Auth_Container_Form_Body_Item">
                                    <div className={`Auth_Container_Form_Body_Item_Content ${errors.fullName ? 'Error' : ''}`}>
                                        <div className={`Auth_Container_Form_Body_Item_Content_Icon`}>
                                            <FiUser size={24} />
                                        </div>
                                        <div className="Auth_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type="text"
                                                placeholder="Họ và Tên"
                                                onChange={(e) => handleChange(e, "fullName")}
                                            />
                                        </div>
                                    </div>
                                    <div className="Auth_Container_Form_Body_Item_Validate">
                                        {errors.fullName}
                                    </div>
                                </div>
                                <div className="Auth_Container_Form_Body_Item">
                                    <div className={`Auth_Container_Form_Body_Item_Content ${errors.userName ? "Error" : ""}`}>
                                        <div className="Auth_Container_Form_Body_Item_Content_Icon">
                                            <FiUserPlus size={24} />
                                        </div>
                                        <div className="Auth_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type="text"
                                                placeholder="Tên đăng nhập"
                                                onChange={(e) => handleChange(e, "userName")}
                                            />
                                        </div>
                                    </div>
                                    <div className="Auth_Container_Form_Body_Item_Validate">{errors.userName}</div>
                                </div>
                                <div className="Auth_Container_Form_Body_Item">
                                    <div className={`Auth_Container_Form_Body_Item_Content ${errors.email ? "Error" : ""}`}>
                                        <div className="Auth_Container_Form_Body_Item_Content_Icon">
                                            <FiMail size={24} />
                                        </div>
                                        <div className="Auth_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type="email"
                                                placeholder="Nhập email"
                                                onChange={(e) => handleChange(e, "email")}
                                            />
                                        </div>
                                    </div>
                                    <div className="Auth_Container_Form_Body_Item_Validate">{errors.email}</div>
                                </div>
                                <div className="Auth_Container_Form_Body_Item">
                                    <div className={`Auth_Container_Form_Body_Item_Content ${errors.password ? "Error" : ""}`}>
                                        <div className="Auth_Container_Form_Body_Item_Content_Icon">
                                            <FiLock size={24} />
                                        </div>
                                        <div className="Auth_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type={openPassword ? "text" : "password"}
                                                placeholder="Nhập mật khẩu"
                                                onChange={(e) => handleChange(e, "password")}
                                            />
                                        </div>
                                        <div onClick={() => handleShowPassword()}
                                             className="Auth_Container_Form_Body_Item_Content_IconEye">
                                            {openPassword ? <FiEye size={20} /> :
                                                <FiEyeOff size={20} />}
                                        </div>
                                    </div>
                                    <div className="Auth_Container_Form_Body_Item_Validate">{errors.password}</div>
                                </div>
                                <div className="Auth_Container_Form_Body_Item">
                                    <div className={`Auth_Container_Form_Body_Item_Content ${errors.confirmPassword ? "Error" : ""}`}>
                                        <div className="Auth_Container_Form_Body_Item_Content_Icon">
                                            <FiLock size={24} />
                                        </div>
                                        <div className="Auth_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type={openPassword ? "text" : "password"}
                                                placeholder="Xác nhận mật khẩu"
                                                onChange={(e) => handleChange(e, "confirmPassword")}
                                            />
                                        </div>
                                        <div onClick={() => handleShowPassword()}
                                             className="Auth_Container_Form_Body_Item_Content_IconEye">
                                            {openPassword ? <FiEye size={20} /> :
                                                <FiEyeOff size={20} />}
                                        </div>
                                    </div>
                                    <div className="Auth_Container_Form_Body_Item_Validate">{errors.confirmPassword}</div>
                                </div>
                            </div>
                            <div className="Auth_Container_Form_Footer">
                                <button onClick={handleSubmit}>
                                    Đăng ký
                                </button>
                                <div className="Auth_Container_Form_Footer_Choice">
                                    <Link to="/">Đã có tài khoản?</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </BrowserView>
            <MobileView>
                <div className="AuthMobile">
                    <div className="AuthMobile_Container">
                        <SloganMobile/>
                        <div className="AuthMobile_Container_Form">
                            <div className="AuthMobile_Container_Form_Header">
                                <h1>Đăng ký</h1>
                            </div>
                            <div className="AuthMobile_Container_Form_Body">
                                <div className="AuthMobile_Container_Form_Body_Item">
                                    <div
                                        className={`AuthMobile_Container_Form_Body_Item_Content ${errors.fullName ? 'Error' : ''}`}>
                                        <div className={`AuthMobile_Container_Form_Body_Item_Content_Icon`}>
                                            <FiUser size={24}/>
                                        </div>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type="text"
                                                placeholder="Họ và Tên"
                                                onChange={(e) => handleChange(e, "fullName")}
                                            />
                                        </div>
                                    </div>
                                    <div className="AuthMobile_Container_Form_Body_Item_Validate">
                                        {errors.fullName}
                                    </div>
                                </div>
                                <div className="AuthMobile_Container_Form_Body_Item">
                                    <div
                                        className={`AuthMobile_Container_Form_Body_Item_Content ${errors.userName ? "Error" : ""}`}>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Icon">
                                            <FiUserPlus size={24}/>
                                        </div>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type="text"
                                                placeholder="Tên đăng nhập"
                                                onChange={(e) => handleChange(e, "userName")}
                                            />
                                        </div>
                                    </div>
                                    <div
                                        className="AuthMobile_Container_Form_Body_Item_Validate">{errors.userName}</div>
                                </div>
                                <div className="AuthMobile_Container_Form_Body_Item">
                                    <div
                                        className={`AuthMobile_Container_Form_Body_Item_Content ${errors.email ? "Error" : ""}`}>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Icon">
                                            <FiMail size={24}/>
                                        </div>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type="email"
                                                placeholder="Nhập email"
                                                onChange={(e) => handleChange(e, "email")}
                                            />
                                        </div>
                                    </div>
                                    <div className="AuthMobile_Container_Form_Body_Item_Validate">{errors.email}</div>
                                </div>
                                <div className="AuthMobile_Container_Form_Body_Item">
                                    <div
                                        className={`AuthMobile_Container_Form_Body_Item_Content ${errors.password ? "Error" : ""}`}>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Icon">
                                            <FiLock size={24}/>
                                        </div>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type={openPassword ? "text" : "password"}
                                                placeholder="Nhập mật khẩu"
                                                onChange={(e) => handleChange(e, "password")}
                                            />
                                        </div>
                                        <div onClick={() => handleShowPassword()}
                                             className="AuthMobile_Container_Form_Body_Item_Content_IconEye">
                                            {openPassword ? <FiEye size={20}/> :
                                                <FiEyeOff size={20}/>}
                                        </div>
                                    </div>
                                    <div
                                        className="AuthMobile_Container_Form_Body_Item_Validate">{errors.password}</div>
                                </div>
                                <div className="AuthMobile_Container_Form_Body_Item">
                                    <div
                                        className={`AuthMobile_Container_Form_Body_Item_Content ${errors.confirmPassword ? "Error" : ""}`}>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Icon">
                                            <FiLock size={24}/>
                                        </div>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type={openPassword ? "text" : "password"}
                                                placeholder="Xác nhận mật khẩu"
                                                onChange={(e) => handleChange(e, "confirmPassword")}
                                            />
                                        </div>
                                        <div onClick={() => handleShowPassword()}
                                             className="AuthMobile_Container_Form_Body_Item_Content_IconEye">
                                            {openPassword ? <FiEye size={20}/> :
                                                <FiEyeOff size={20}/>}
                                        </div>
                                    </div>
                                    <div
                                        className="AuthMobile_Container_Form_Body_Item_Validate">{errors.confirmPassword}</div>
                                </div>
                            </div>
                            <div className="AuthMobile_Container_Form_Footer">
                                <button onClick={handleSubmit}>
                                    Đăng ký
                                </button>
                                <div className="AuthMobile_Container_Form_Footer_Choice">
                                    <div onClick={()=> navigate("/")}>Đã có tài khoản?</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </MobileView>
        </>
    )
}

export default Register;