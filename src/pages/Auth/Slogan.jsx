import Logo from "../../assets/images/logo.png";

const Slogan = () => {
    return (
        <>
            <div className="Auth_Container_Slogan">
                <div className="Auth_Container_Slogan_Logo">
                    <img src={Logo} alt=""/>
                </div>
                <div className="Auth_Container_Slogan_Body">
                    <h3>SmartLight</h3>
                    <h4>Ánh sáng trong tầm tay</h4>
                </div>
                <div className="Auth_Container_Slogan_Image"></div>
            </div>
        </>
    );
}
export default Slogan;