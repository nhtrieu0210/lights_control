import {BrowserView, MobileView} from "react-device-detect";
import {FiMail} from "react-icons/fi";
import {Link} from "react-router-dom";
import {CiBarcode} from "react-icons/ci";
import Slogan from "./Slogan";
import {useState} from "react";
import {handleValidateEmail} from "../../Validation";
import SloganMobile from "./SloganMobile";

const ForgotPassword = () => {
    const [formData, setFormData] = useState({
        email: "",
        otp: ""
    })
    const [errors, setErrors] = useState({
        email: "",
        otp: ""
    })

    const handleChange = (e, name) => {
        let value = e.target.value;
        setFormData({
            ...formData,
            [name]: value
        })
    }
    const handleSubmit = () => {
        const checkEmail = handleValidateEmail(formData.email);

        setErrors({
            email: checkEmail,
        })
    }


    return (
        <>
            <BrowserView>
                <div className="Auth">
                    <div className="Auth_Container">
                        <Slogan />
                        <div className="Auth_Container_Form">
                            <div className="Auth_Container_Form_Header">
                                <h1>Xác nhận Email</h1>
                            </div>
                            <div className="Auth_Container_Form_Body">
                                <div className="Auth_Container_Form_Body_Item">
                                    <div className={`Auth_Container_Form_Body_Item_Content ${errors.email ? 'Error' : ''}`}>
                                        <div className="Auth_Container_Form_Body_Item_Content_Icon">
                                            <FiMail color={errors.email ? 'red' : 'black'} size={24} />
                                        </div>
                                        <div className="Auth_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type="text"
                                                placeholder="Email của bạn"
                                                onChange={(e) => handleChange(e, "email")}
                                            />
                                        </div>
                                    </div>
                                    <div className="Auth_Container_Form_Body_Item_Validate">{errors.email}</div>
                                </div>
                                <div className="Auth_Container_Form_Body_Item">
                                <div className={`Auth_Container_Form_Body_Item_Content ${errors.otp ? 'Error' : ''}`}>
                                        <div className="Auth_Container_Form_Body_Item_Content_Icon">
                                            <CiBarcode color={errors.otp ? 'red' : 'black'} size={24} />
                                        </div>
                                        <div className="Auth_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type="text"
                                                placeholder="OTP của bạn"
                                                onChange={(e) => handleChange(e, "otp")}
                                            />
                                        </div>
                                    </div>
                                    <div className="Auth_Container_Form_Body_Item_Validate">{errors.otp}</div>
                                </div>
                                <div className="Auth_Container_Form_Body_Item">
                                <div className="Auth_Container_Form_Body_Item_OTP">
                                        Gửi mã OTP
                                    </div>
                                </div>
                            </div>
                            <div className="Auth_Container_Form_Footer">
                                <button>
                                    Xác nhận
                                </button>
                                <div className="Auth_Container_Form_Footer_Choice">
                                    <Link to="/">Quay lại trang đăng nhập.</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </BrowserView>
            <MobileView>
                <div className="AuthMobile">
                    <div className="AuthMobile_Container">
                        <SloganMobile/>
                        <div className="AuthMobile_Container_Form">
                            <div className="AuthMobile_Container_Form_Header">
                                <h1>Xác nhận Email</h1>
                            </div>
                            <div className="AuthMobile_Container_Form_Body">
                                <div className="AuthMobile_Container_Form_Body_Item">
                                    <div
                                        className={`AuthMobile_Container_Form_Body_Item_Content ${errors.email ? 'Error' : ''}`}>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Icon">
                                            <FiMail color={errors.email ? 'red' : 'black'} size={24}/>
                                        </div>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type="text"
                                                placeholder="Email của bạn"
                                                onChange={(e) => handleChange(e, "email")}
                                            />
                                        </div>
                                    </div>
                                    <div className="AuthMobile_Container_Form_Body_Item_Validate">{errors.email}</div>
                                </div>
                                <div className="AuthMobile_Container_Form_Body_Item">
                                    <div
                                        className={`AuthMobile_Container_Form_Body_Item_Content ${errors.otp ? 'Error' : ''}`}>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Icon">
                                            <CiBarcode color={errors.otp ? 'red' : 'black'} size={24}/>
                                        </div>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type="text"
                                                placeholder="OTP của bạn"
                                                onChange={(e) => handleChange(e, "otp")}
                                            />
                                        </div>
                                    </div>
                                    <div className="AuthMobile_Container_Form_Body_Item_Validate">{errors.otp}</div>
                                </div>
                                <div className="AuthMobile_Container_Form_Body_Item">
                                    <div className="AuthMobile_Container_Form_Body_Item_OTP">
                                        Gửi mã OTP
                                    </div>
                                </div>
                            </div>
                            <div className="AuthMobile_Container_Form_Footer">
                                <button onClick={()=>handleSubmit()}>
                                    Xác nhận
                                </button>
                                <div className="AuthMobile_Container_Form_Footer_Choice">
                                    <Link to="/">Quay lại trang đăng nhập.</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </MobileView>
        </>
    )
}

export default ForgotPassword;