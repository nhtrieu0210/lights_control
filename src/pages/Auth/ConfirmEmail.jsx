import {BrowserView, MobileView} from "react-device-detect";
import {FiMail} from "react-icons/fi";
import {Link} from "react-router-dom";
import Slogan from "./Slogan";
import {useState} from "react";
import {handleValidateEmail} from "../../Validation";
import SloganMobile from "./SloganMobile";

const ConfirmEmail = () => {
    const [email, setEmail] = useState("");
    const [error, setError] = useState("")
    const handleSubmit = () => {
        setError(handleValidateEmail(email));
    }
    return (
        <>
            <BrowserView>
                <div className="Auth">
                    <div className="Auth_Container">
                        <Slogan />
                        <div className="Auth_Container_Form">
                            <div className="Auth_Container_Form_Header">
                                <h1>Xác nhận Email</h1>
                            </div>
                            <div className="Auth_Container_Form_Body">
                                <div className="Auth_Container_Form_Body_Item">
                                    <div className={`Auth_Container_Form_Body_Item_Content ${error ? "Error" : ""}`}>
                                        <div className="Auth_Container_Form_Body_Item_Content_Icon">
                                            <FiMail color={`${error ? 'red' : 'black'}`} size={24} />
                                        </div>
                                        <div className="Auth_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type="text"
                                                placeholder="Email của bạn"
                                                onChange={(e)=> setEmail(e.target.value)}
                                            />
                                        </div>
                                    </div>
                                    <div className="Auth_Container_Form_Body_Item_Validate">{error}</div>
                                </div>
                            </div>
                            <div className="Auth_Container_Form_Footer">
                                <button onClick={() => handleSubmit()}>
                                    Xác nhận email
                                </button>
                                <div className="Auth_Container_Form_Footer_Choice">
                                    <Link to="/register">Quay lại trang đăng ký.</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </BrowserView>
            <MobileView>
                <div className="AuthMobile">
                    <div className="AuthMobile_Container">
                        <SloganMobile/>
                        <div className="AuthMobile_Container_Form">
                            <div className="AuthMobile_Container_Form_Header">
                                <h1>Xác nhận Email</h1>
                            </div>
                            <div className="AuthMobile_Container_Form_Body">
                                <div className="AuthMobile_Container_Form_Body_Item">
                                    <div
                                        className={`AuthMobile_Container_Form_Body_Item_Content ${error ? "Error" : ""}`}>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Icon">
                                            <FiMail color={`${error ? 'red' : 'black'}`} size={24}/>
                                        </div>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Input">
                                            <input
                                                type="text"
                                                placeholder="Email của bạn"
                                                onChange={(e) => setEmail(e.target.value)}
                                            />
                                        </div>
                                    </div>
                                    <div className="AuthMobile_Container_Form_Body_Item_Validate">{error}</div>
                                </div>
                            </div>
                            <div className="AuthMobile_Container_Form_Footer">
                                <button onClick={() => handleSubmit()}>
                                    Xác nhận email
                                </button>
                                <div className="AuthMobile_Container_Form_Footer_Choice">
                                    <Link to="/register">Quay lại trang đăng ký.</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </MobileView>
        </>
    )
}

export default ConfirmEmail;