import Logo from "../../assets/images/logo.png";

const SloganMobile = () => {
    return (
        <>
            <div className="AuthMobile_Container_Slogan">
                <div className="AuthMobile_Container_Slogan_Logo">
                    <img src={Logo} alt=""/>
                </div>
                <div className="AuthMobile_Container_Slogan_Body">
                    <h3>SmartLight</h3>
                    <h4>Ánh sáng trong tầm tay</h4>
                </div>
                <div className="AuthMobile_Container_Slogan_Image"></div>
            </div>
        </>
    );
}
export default SloganMobile;