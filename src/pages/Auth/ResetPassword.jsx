import {BrowserView, MobileView} from "react-device-detect";
import {FiEye, FiEyeOff, FiLock} from "react-icons/fi";
import {Link} from "react-router-dom";
import {useState} from "react";
import Slogan from "./Slogan";
import {handleValidateConfirmPassword, handleValidatePassword} from "../../Validation";
import SloganMobile from "./SloganMobile";

const ResetPassword = () => {
    const [formData, setFormData] = useState({
        password: "",
        confirmPassword: ""
    })
    const [errors, setErrors] = useState({
        password: "",
        confirmPassword: ""
    })
    const [openPassword, setOpenPassword] = useState(false);
    const handleShowPassword = () => {
        setOpenPassword(!openPassword);
    }
    const handleChange = (e, name) => {
        const value = e.target.value;
        setFormData({
            ...formData,
            [name]:value
        })
    }

    const handleSubmit = () => {
        const checkPassword = handleValidatePassword(formData.password);
        const checkConfirmPassword = handleValidateConfirmPassword(formData.password, formData.confirmPassword)
        setErrors({
            password: checkPassword,
            confirmPassword: checkConfirmPassword
        })
    }

    return (
        <>
            <BrowserView>
                <div className="Auth">
                    <div className="Auth_Container">
                        <Slogan />
                        <div className="Auth_Container_Form">
                            <div className="Auth_Container_Form_Body">
                                <div className="Auth_Container_Form_Body_Item">
                                    <div className={`Auth_Container_Form_Body_Item_Content ${errors.password ? "Error" : ""}`}>
                                        <div className="Auth_Container_Form_Body_Item_Content_Icon">
                                            <FiLock color={errors.password ? "red" : "black"}  size={24}/>
                                        </div>
                                        <div className="Auth_Container_Form_Body_Item_Content_Input">
                                            <input type={openPassword ? "text" : "password"}
                                                   placeholder="Nhập mật khẩu mới"
                                                   onChange={(e) => handleChange(e, "password")}
                                            />
                                        </div>
                                        <div onClick={() => handleShowPassword()}
                                             className="Auth_Container_Form_Body_Item_Content_IconEye">
                                            {openPassword ? <FiEyeOff size={20}/> :
                                                <FiEye size={20}/>}
                                        </div>
                                    </div>
                                    <div className="Auth_Container_Form_Body_Item_Validate">
                                        {errors.password}
                                    </div>
                                </div>
                                <div className="Auth_Container_Form_Body_Item">
                                    <div className={`Auth_Container_Form_Body_Item_Content ${errors.confirmPassword ? "Error" : ""}`}>
                                        <div className="Auth_Container_Form_Body_Item_Content_Icon">
                                            <FiLock color={errors.confirmPassword ? "red" : "black"} size={24}/>
                                        </div>
                                        <div className="Auth_Container_Form_Body_Item_Content_Input">
                                            <input type={openPassword ? "text" : "password"}
                                                   placeholder="Xác nhận mật khẩu mới"
                                                   onChange={(e) => handleChange(e, "confirmPassword")}
                                            />
                                        </div>
                                        <div onClick={() => handleShowPassword()}
                                             className="Auth_Container_Form_Body_Item_Content_IconEye">
                                            {openPassword ? <FiEye size={20}/> :
                                                <FiEyeOff size={20}/>}
                                        </div>
                                    </div>
                                    <div className="Auth_Container_Form_Body_Item_Validate">
                                        {errors.confirmPassword}
                                    </div>
                                </div>
                            </div>
                            <div className="Auth_Container_Form_Footer">
                                <button onClick={() => handleSubmit()}>
                                    Xác nhận
                                </button>
                                <div className="Auth_Container_Form_Footer_Choice">
                                    <Link to="/register">Quay lại trang đăng ký.</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </BrowserView>
            <MobileView>
                <div className="AuthMobile">
                    <div className="AuthMobile_Container">
                        <SloganMobile/>
                        <div className="AuthMobile_Container_Form">
                            <div className="AuthMobile_Container_Form_Body">
                                <div className="AuthMobile_Container_Form_Body_Item">
                                    <div
                                        className={`AuthMobile_Container_Form_Body_Item_Content ${errors.password ? "Error" : ""}`}>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Icon">
                                            <FiLock color={errors.password ? "red" : "black"} size={24}/>
                                        </div>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Input">
                                            <input type={openPassword ? "text" : "password"}
                                                   placeholder="Nhập mật khẩu mới"
                                                   onChange={(e) => handleChange(e, "password")}
                                            />
                                        </div>
                                        <div onClick={() => handleShowPassword()}
                                             className="AuthMobile_Container_Form_Body_Item_Content_IconEye">
                                            {openPassword ? <FiEyeOff size={20}/> :
                                                <FiEye size={20}/>}
                                        </div>
                                    </div>
                                    <div className="AuthMobile_Container_Form_Body_Item_Validate">
                                        {errors.password}
                                    </div>
                                </div>
                                <div className="AuthMobile_Container_Form_Body_Item">
                                    <div
                                        className={`AuthMobile_Container_Form_Body_Item_Content ${errors.confirmPassword ? "Error" : ""}`}>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Icon">
                                            <FiLock color={errors.confirmPassword ? "red" : "black"} size={24}/>
                                        </div>
                                        <div className="AuthMobile_Container_Form_Body_Item_Content_Input">
                                            <input type={openPassword ? "text" : "password"}
                                                   placeholder="Xác nhận mật khẩu mới"
                                                   onChange={(e) => handleChange(e, "confirmPassword")}
                                            />
                                        </div>
                                        <div onClick={() => handleShowPassword()}
                                             className="AuthMobile_Container_Form_Body_Item_Content_IconEye">
                                            {openPassword ? <FiEye size={20}/> :
                                                <FiEyeOff size={20}/>}
                                        </div>
                                    </div>
                                    <div className="AuthMobile_Container_Form_Body_Item_Validate">
                                        {errors.confirmPassword}
                                    </div>
                                </div>
                            </div>
                            <div className="AuthMobile_Container_Form_Footer">
                                <button onClick={() => handleSubmit()}>
                                    Xác nhận
                                </button>
                                <div className="AuthMobile_Container_Form_Footer_Choice">
                                    <Link to="/register">Quay lại trang đăng ký.</Link>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </MobileView>
        </>
    )
}

export default ResetPassword;