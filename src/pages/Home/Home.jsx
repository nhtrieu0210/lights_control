import "./Home.scss";
import {FiPlus, FiSearch} from "react-icons/fi";
import Device from "../../components/Device/Device";
import AddDevice from "../../components/AddDevice/AddDevice";
import {useState} from "react";

const  Home = () => {
    const [isOpen, setIsOpen] = useState(false);
    const handleClose = () => {
        setIsOpen(false);
    }
    return (
        <div className="Home">
            <div className="Home_Container">
                <div className="Home_Container_Header">
                    <h2 className="Home_Container_Header_Title">Devices</h2>
                    <div className="Home_Container_Header_Tool">
                        <div className="Home_Container_Header_Tool_Search">
                            <input type="text" placeholder="Device's name..."/>
                            <FiSearch/>
                        </div>
                        <div className="Home_Container_Header_Tool_Add">
                            <div onClick={() => setIsOpen(true)} className="Home_Container_Header_Tool_Add_Btn">
                                <FiPlus />
                            </div>
                            {isOpen ?
                                <div className="Home_Container_Header_Tool_Add_Popup">
                                    <AddDevice handleClick={handleClose} />
                                </div> : <></>
                            }
                        </div>
                    </div>
                </div>
                <div className="Home_Container_Body">
                    <div className="Home_Container_Body_Content">
                        <Device />
                        <Device />
                        <Device />
                        <Device />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;