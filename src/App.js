import "./index.scss";

import {BrowserRouter as Router, Navigate, Route, Routes} from "react-router-dom";

import Login from "./pages/Auth/Login";
import Register from "./pages/Auth/Register";
import ResetPassword from "./pages/Auth/ResetPassword";
import ConfirmEmail from "./pages/Auth/ConfirmEmail";
import ForgotPassword from "./pages/Auth/ForgotPassword";
import Home from "./pages/Home/Home";
import Header from "./components/Header/Header";
import Settings from "./pages/Settings/Settings";

import {ToastContainer} from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Control from "./pages/Control/Control";
import Admin from "./pages/Admin/Admin";
import {useContext, useEffect, useState} from "react";
import {AuthContext} from "./AuthContext";

function App() {
    const [isLogin, setIsLogin] = useState(false);
    const context = useContext(AuthContext);
    useEffect(() => {
        setIsLogin(context.isLoggedIn)
        console.log(isLogin)
    });
  return (
          <div className="App">
              <Router>
                  {isLogin ?
                      <div className="Layout">
                          <Header/>
                          <div className="Layout_Container">
                              <Routes>
                                  <Route exact path="/" element={<Home />}/>
                                  <Route exact path="/control" element={<Control/>}/>
                                  <Route exact path="/settings" element={<Settings/>}/>
                                  <Route exact path="/admin" element={<Admin/>}/>
                              </Routes>
                          </div>
                      </div>
                      :
                      <div className="Layout">
                          <div className="Layout_Container">
                              <Routes>
                                  <Route exact path="/" element={<Login/>}/>
                                  <Route exact path="/register" element={<Register/>}/>
                                  <Route exact path="/reset-password" element={<ResetPassword/>}/>
                                  <Route exact path="/confirm-email" element={<ConfirmEmail/>}/>
                                  <Route exact path="/forgot-password" element={<ForgotPassword/>}/>
                              </Routes>
                          </div>
                      </div>
                  }
                  <ToastContainer
                      autoClose={1000}
                      theme='colored'
                  />
              </Router>
          </div>
  );
}

export default App;
