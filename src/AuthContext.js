import {createContext, useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";

const AuthContext = createContext();


const AuthProvider = ({children}) => {

    const [isLoggedIn, setIsLoggedIn] = useState();

    const Login = (saveAccount) => {
        if(saveAccount) {
            localStorage.setItem('isLoggedIn', 'true');
            setIsLoggedIn(true);
        } else {
            sessionStorage.setItem('isLoggedIn', 'true');
            setIsLoggedIn(true);
        }
    }

    const Logout = () => {
        localStorage.removeItem('isLoggedIn');
        sessionStorage.removeItem('isLoggedIn');
        setIsLoggedIn(false);
    }

    const checkLogin = () => {
        const local = localStorage.getItem('isLoggedIn');
        const session = sessionStorage.getItem('isLoggedIn');
        if(local || session) {
            setIsLoggedIn(true);
        } else {
            setIsLoggedIn(false);
        }
    }

    useEffect(() => {
        checkLogin();
    }, [isLoggedIn]);

    const value = {
        isLoggedIn,
        Login,
        Logout
    }

    return (
        <AuthContext.Provider value={value}>
            {children}
        </AuthContext.Provider>
    )
}

export {AuthProvider, AuthContext};