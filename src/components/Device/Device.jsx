import "./Device.scss";
import {useNavigate} from "react-router-dom";
import {FiShare2, FiSliders, FiTrash2, FiTrendingUp} from "react-icons/fi";
const Device = () => {
    const navigate = useNavigate();
    return (
        <>
            <div className="Device">
                <div className="Device_Header">
                    <div className="Device_Header_Content">
                        <div className="Device_Header_Content_NameId">
                            <div className="Device_Header_Content_NameId_Id">
                                T0210
                            </div>
                             <span> - </span>
                            <div className="Device_Header_Content_NameId_Name">
                                Bedroom Light
                            </div>
                        </div>
                        <div className="Device_Header_Content_Desc">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat, repellendus.
                        </div>
                    </div>
                    <div className="Device_Header_Status">
                        <div className="Device_Header_Status_Dot Connect"></div>
                        <div className="Device_Header_Status_Text">Connection</div>
                    </div>
                </div>
                <div className="Device_Footer">
                    <div className="Device_Footer_Control">
                        <button onClick={() => navigate("/control")} className="Device_Footer_Control_Btn"><FiSliders /></button>
                    </div>
                    <div className="Device_Footer_Function">
                        <button className="Device_Footer_Function_Btn Primary"><FiTrendingUp /></button>
                        <button className="Device_Footer_Function_Btn Primary"><FiShare2 /></button>
                        <button className="Device_Footer_Function_Btn Danger"><FiTrash2 /></button>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Device;