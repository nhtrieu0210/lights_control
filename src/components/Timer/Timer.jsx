import "./Timer.scss";
import {FiX} from "react-icons/fi";

const Timer = ({handleClose}) => {
    return (
        <>
            <div className="Timer">
                <div className="Timer_Container">
                    <div className="Timer_Container_Header">
                        <h3 className="Timer_Container_Header_Title">
                            Timer
                        </h3>
                        <div className="Timer_Container_Header_Btn">
                            <FiX onClick={handleClose}/>
                        </div>
                    </div>
                    <div className="Timer_Container_Body">
                        <div className="Timer_Container_Body_Item">
                            <div className="Timer_Container_Body_Item_Title">
                                <input type="radio" name="flash"/>
                                <label htmlFor="flash">Flash Time</label>
                            </div>
                            <div className="Timer_Container_Body_Item_Time">
                                <div className="Timer_Container_Body_Item_Time_Item">
                                    <label htmlFor="hours">On</label>
                                    <input type="number" name="Second"/>
                                </div>
                                <div className="Timer_Container_Body_Item_Time_Item">
                                    <label htmlFor="hours">Off</label>
                                    <input type="number" name="Second"/>
                                </div>
                                <div className="Timer_Container_Body_Item_Time_Item">
                                    <label htmlFor="hours">Loop</label>
                                    <input type="number" name="Loop"/>
                                </div>
                            </div>
                        </div>
                        <div className="Timer_Container_Body_Item">
                            <div className="Timer_Container_Body_Item">
                                <div className="Timer_Container_Body_Item_Title">
                                    <input type="radio" name="flash"/>
                                    <label htmlFor="flash">schedule</label>
                                </div>
                                <div className="Timer_Container_Body_Item_Time">
                                    <div className="Timer_Container_Body_Item_Time_Item">
                                        <label htmlFor="hours">On</label>
                                        <input type="number" name="Second"/>
                                    </div>
                                    <div className="Timer_Container_Body_Item_Time_Item">
                                        <label htmlFor="hours">Off</label>
                                        <input type="number" name="Second"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Timer;