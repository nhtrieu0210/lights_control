import "./Header.scss";
import Logo from "../../assets/images/logo.png"
import Profile from "../../assets/images/profile-1.png"
import {FiClock, FiGrid, FiLogOut, FiSettings} from "react-icons/fi";
import {useNavigate} from "react-router-dom";
import Timer from "../Timer/Timer";
import {useContext, useState} from "react";
import {AuthContext} from "../../AuthContext";

const Header = () => {
    const [activeHome, setActiveHome] = useState(false)
    const [activeTime, setActiveTime] = useState(false)
    const [isOpen, setIsOpen] = useState(false)
    const [isOpenProf, setIsOpenProf] = useState(false)
    const navigate = useNavigate();
    const handleClose = () => {
        setIsOpen(false);   
    }
    const handleOpenProf = () => {
        setIsOpenProf(!isOpenProf);
    }

    const context = useContext(AuthContext);
    const handleLogout = () => {
        context.Logout();
        navigate("/")
    }

    const handleActiveHome = () => {
        setActiveHome(true);
        setActiveTime(false)
    }
    const handleActiveTime = () => {
        setActiveTime(true)
        setActiveHome(false);
    }

    return (
        <>
            <div className="Header">
                <div className="Header_Logo" onClick={() => navigate("/")}>
                    <img className="Header_Logo_Img" src={Logo} alt=""/>
                    <div className="Header_Logo_Brand">Smartlight</div>
                </div>
                <div className="Header_Center">
                    <ul className="Header_Center_List">
                        <li onClick={()=> handleActiveHome()} className={`Header_Center_List_Item ${activeHome ? "Active" : ""}`}>
                            <div onClick={() => navigate("/")} className="Header_Center_List_Item_Btn">
                                <FiGrid/>
                            </div>
                        </li>
                        <li onClick={()=> handleActiveTime()} className={`Header_Center_List_Item ${activeTime ? "Active" : ""}`}>
                            <div className="Header_Center_List_Item_Btn" onClick={() => setIsOpen(true)}>
                                <FiClock/>
                            </div>
                            {isOpen ? <Timer handleClose={handleClose}/> : <></>}
                        </li>
                    </ul>
                </div>
                <div className="Header_Right">
                    <ul className="Header_Right_List">
                        <li className="Header_Right_List_Item">
                            <div className="Header_Right_List_Item_Btn" onClick={() => navigate("/settings")}>
                                <FiSettings />
                            </div>
                        </li>
                        <li className="Header_Right_List_Item">
                            <div onClick={() => handleOpenProf()} className="Header_Right_List_Item_Btn">
                                <img
                                    className="Header_Right_List_Item_Btn_Img"
                                    src={Profile} alt=""
                                />
                            </div>
                            {
                                isOpenProf ? <div className="Header_Right_List_Item_Profile">
                                    <div className="Header_Right_List_Item_Profile_Header">
                                        <div className="Header_Right_List_Item_Profile_Header_Img">
                                            <img src={Profile} alt=""/>
                                        </div>
                                        <div className="Header_Right_List_Item_Profile_Header_Content">
                                            <div className="Header_Right_List_Item_Profile_Header_Content_Name">John
                                                Doe
                                            </div>
                                            <div className="Header_Right_List_Item_Profile_Header_Content_Role">nhtrieu@gmail.com
                                            </div>
                                        </div>
                                    </div>
                                    <div className="Header_Right_List_Item_Profile_Footer">
                                        <div onClick={() => handleLogout()}
                                             className="Header_Right_List_Item_Profile_Footer_Item">
                                            <div className="Header_Right_List_Item_Profile_Footer_Item_Icon">
                                                <FiLogOut/>
                                            </div>
                                            <div className="Header_Center_List_Item_Profile_Footer_Item_Text">Log out
                                            </div>
                                        </div>
                                    </div>
                                </div> : <></>
                            }
                        </li>
                    </ul>
                </div>
            </div>
        </>
    );
};
export default Header;