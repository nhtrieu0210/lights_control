import {FiX} from "react-icons/fi";
import "./AddUser.scss"

const AddUser = ({handleClose}) => {
    return (
        <>
            <div className="AddUser">
                <div className="AddUser_Title">
                    <h3>Add User</h3>
                    <FiX onClick={handleClose}/>
                </div>
                <div className="AddUser_Body">
                    <div className="AddUser_Body_Item">
                        <div className="AddUser_Body_Item_Title">Full Name</div>
                        <div className="AddUser_Body_Item_Input">
                            <input type="text" placeholder="Full name..."/>
                        </div>
                    </div>
                    <div className="AddUser_Body_Item">
                        <div className="AddUser_Body_Item_Title">User Name</div>
                        <div className="AddUser_Body_Item_Input">
                            <input type="text" placeholder="User name..."/>
                        </div>
                    </div>
                    <div className="AddUser_Body_Item">
                        <div className="AddUser_Body_Item_Title">Email</div>
                        <div className="AddUser_Body_Item_Input">
                            <input type="text" placeholder="Email..."/>
                        </div>
                    </div>
                    <div className="AddUser_Body_Item">
                        <div className="AddUser_Body_Item_Title">Password</div>
                        <div className="AddUser_Body_Item_Input">
                            <input type="password" placeholder="Password..."/>
                        </div>
                    </div>
                    <div className="AddUser_Body_Btn">
                        <button className="Primary">Add user</button>
                        <button className="Danger">Clear</button>
                    </div>
                </div>
            </div>
        </>
    )
}
export default AddUser;