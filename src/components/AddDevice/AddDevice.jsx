import "./AddDevice.scss";
import {FiX} from "react-icons/fi";

const AddDevice = ({handleClick}) => {
    return (
        <>
            <div className="AddDevice">
                <div className="AddDevice_Title">
                    <h3>Add device</h3>
                    <FiX onClick={handleClick}/>
                </div>
                <div className="AddDevice_Body">
                    <div className="AddDevice_Body_Item">
                        <div className="AddDevice_Body_Item_Title">Device ID</div>
                        <div className="AddDevice_Body_Item_Input">
                            <input type="text" placeholder="Device ID..."/>
                        </div>
                    </div>
                    <div className="AddDevice_Body_Item">
                        <div className="AddDevice_Body_Item_Title">Device name</div>
                        <div className="AddDevice_Body_Item_Input">
                            <input type="text" placeholder="Device name..."/>
                        </div>
                    </div>
                    <div className="AddDevice_Body_Item">
                        <div className="AddDevice_Body_Item_Title">Device Description</div>
                        <div className="AddDevice_Body_Item_Input">
                            <input type="text" placeholder="Device description..."/>
                        </div>
                    </div>
                    <div className="AddDevice_Body_Btn">
                        <button>Add Device</button>
                    </div>
                </div>
            </div>
        </>
    );
};
export default AddDevice;