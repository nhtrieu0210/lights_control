
export const handleValidateName = (name) => {
    let re = /^[a-zA-ZÀ-Ỹà-ỹ\s]+$/;
    if(!name) {
        return "Họ tên không được để trống!";
    }
    if(name.length < 6) {
        return "Họ tên phải ít nhất 6 ký tự!";
    }
    if(!re.test(String(name).toLowerCase())) {
        return "Họ tên không được chứa ký tự số!";
    }
    return "";
}

export const handleValidateUsername = (name) => {
    if(!name) {
        return "Tài khoản không được để trống!";
    }
    if(name.length < 6) {
        return "Tài khoản phải ít nhất 6 ký tự!";
    }
    return "";
}
export const handleValidatePassword = (pass) => {
    if(!pass) {
        return "Mật khẩu không được để trống!";
    }
    if(pass.length < 6) {
        return "Mật khẩu phải ít nhất 6 ký tự!";
    }
    return "";
}

export const handleValidateConfirmPassword = (pass, pass2) => {
    if(!pass) {
        return "Mật khẩu không được để trống!";
    }
    if(pass !== pass2) {
        return "Mật khẩu phải trùng khớp!";
    }
    return "";
}

export const handleValidateEmail = (email) => {
    let re = /^^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    if(!email) {
        return "Email không được để trống!"
    }
    if(!re.test(String(email).toLowerCase())) {
        return "Email không hợp lệ!"
    }
    return "";
}